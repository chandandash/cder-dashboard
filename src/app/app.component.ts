import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularNeo4jService } from 'angular-neo4j';
import { NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {
  showSideNav: boolean = true;

  constructor(private router: Router, private _neo4j: AngularNeo4jService) { }

  ngOnInit() {
    this.connectDb();

    this.router.events
      .subscribe((event) => {

        if (event instanceof NavigationStart) {

          if ((<NavigationStart>event).url === '/') {
            this.router.navigate(['/dashboard']);
          } else {
          }
        }
      });
  }

  connectDb() {
    const url = 'bolt://localhost:7687';
    const username = 'neo4j';
    const password = 'Ronaldo_07';
    const encrypted = true;

    this._neo4j
      .connect(
        url,
        username,
        password,
        encrypted
      )
      .then(driver => {
        if (driver) {
          console.log(`Successfully connected to ${url}`);
        }
      });

      const query = 'MATCH (n) RETURN n LIMIT 5';
      // this._neo4j.run(query).then( res => {
      //   console.log(res);
      // });

    }

  ngOnDestroy() {
    console.log("Disconnecting Database");
    this._neo4j.disconnect();
    
  }
}
