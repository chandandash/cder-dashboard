import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  @Output() showSideNav = new EventEmitter<boolean>();

  isShow: boolean = true;

  constructor() {

  }

  ngOnInit() {
    console.log("Toolbar Initialized")
  }

  toggleSideNav() {
    this.showSideNav.emit(this.isShow);
  }
}
