import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/DataService';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource, PageEvent } from "@angular/material";
import { tap } from "rxjs/operators";
import * as d3 from 'd3';
import * as popoto from 'popoto';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @ViewChild('popotoGraph') graph;

  ngOnInit() {
    this.getGraphData();
    this.plotGraph();

  }

  getGraphData() {
    popoto.rest.CYPHER_URL = "http://localhost:7474/db/data/transaction/commit";
    popoto.rest.AUTHORIZATION = "Basic " + btoa("neo4j:Ronaldo_07");

    popoto.provider.node.Provider = {
      "Facility": {
        "returnAttributes": ["facilityName", "addressCity", "supplyChainRole"],
        "constraintAttribute": "facilityName"
      },
      "Address": {
        "returnAttributes": ["addressLine1","addressCity","addressCountry"],
        "constraintAttribute": "addressLine1"
      },
      "FEI": {
        "returnAttributes": ["feiAddress1", "feiCity", "feiCountry"],
        "constraintAttribute": "feiAddress1"
      },
      "Business": {
        "returnAttributes": ["businessName", ],
        "constraintAttribute": "businessName"
      },
      "SupplyChain": {
        "returnAttributes": ["supplyChainQualifier", "supplyChainRole", "supplyChainStatus"],
        "constraintAttribute": "supplyChainQualifier"
      },
      "FacilityStatus": {
        "returnAttributes": ["facilityName"],
        "constraintAttribute": "facilityName"
      },

    };
  }

  plotGraph() {
    popoto.start("Facility");
  }

}
