import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from "@angular/flex-layout";
import {
  MatToolbarModule,
  MatButtonModule,
  MatBadgeModule,
  MatIconModule,
  MatTabsModule,
  MatCardModule,
  MatTableModule,
  MatDividerModule,
  MatTooltipModule,
  MatSidenavModule,
  MatTreeModule,
  MatListModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatSelectModule,
  MatInputModule,
  MatRadioModule,
  MatChipsModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatAutocompleteModule,
  MatSlideToggleModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSortModule,
  MatMenuModule

} from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AngularNeo4jModule } from 'angular-neo4j';


import { Page404Component } from './404/page-404.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DataService } from './services/DataService';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const appRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: '**', component: Page404Component },
];


@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    Page404Component,
    DashboardComponent,
  ],

  imports: [
    BrowserModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatBadgeModule,
    MatIconModule,
    MatTabsModule,
    MatCardModule,
    MatTableModule,
    MatDividerModule,
    MatTooltipModule,
    MatSidenavModule,
    RouterModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTreeModule,
    MatListModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatRadioModule,
    MatChipsModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    FormsModule, ReactiveFormsModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSortModule,
    MatExpansionModule,
    MatMenuModule,
    AngularNeo4jModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )


  ],
  providers: [DataService, ],
  bootstrap: [AppComponent],
  entryComponents: [],
})
export class AppModule { }
